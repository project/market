This project space is for an old project that didn't work very well.

I'm using the lessons from that version to pivot and build new, better tools
toward the same goals. 

Folks interested in Drupal can try the new system at http://beta.openpredictionmarkets.org

The underlying code will be posted to drupal.org shortly. 
If you want a copy beforehand just get in touch.






+++++++++++++ Some old documentation that might get incorporated into the new module++++++++++++
This module provides markets to Drupal websites.  Markets allow users
to trade things with each other in exchange for points.  You can and
probably should think of points in terms of money.  One prominent use
of this module is to create "prediction markets" or "idea futures"
based on a continuous double action system modeled after the Iowa
Electronic Markets.  These markets are useful to aggregate the
knowledge of many separate individuals on one specific topic.

One goal of the module is to be general enough that it could also be
used to create other market systems like "cap and trade" systems.
These allow large companies or organizations to create internal 
markets for non-monetary goods that have no outside markets such as 
pollution credits.

## Suggested documentation for your users (add this to your about page): ##
<strong>What is This?</strong>
This is a prediction market which can be used to pull together knowledge about a topic and provide a simple answer to complex questions.

It is also simply a way for knowledgeable people to get fame, glory, points, (maybe money), in exchange for placing trades on specific outcomes.


When people make purchases based on their beliefs, the markets often become accurate representations of the likelihood of the outcomes.


For more information, be sure to check the project homepage at
http://openpredictionmarkets.org/

Module Author: Greg Knaddison @greggles
http://drupal.org/user/36762

